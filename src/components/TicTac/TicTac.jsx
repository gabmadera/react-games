import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  startGame,
  selectGame,
  selectTurn,
  selectBoard,
  setX,
  checkWin,
  selectScoreO,
  selectScoreX,
  resetScores,
  selectTie,
  selectIsWinner,
} from '../../features/tictacSlice';

import './TicTac.scss';

export default function TicTac() {
  const gameStart = useSelector(selectGame);
  const currentTurn = useSelector(selectTurn);
  const board = useSelector(selectBoard);
  const dispatch = useDispatch();
  const scoreO = useSelector(selectScoreO);
  const scoreX = useSelector(selectScoreX);
  const tie = useSelector(selectTie);
  const winner = useSelector(selectIsWinner);

  useEffect(() => {
    dispatch(checkWin());
  }, [currentTurn, dispatch]);

  return (
    <div className="TicTac">
      <h1 className="TicTac__title">Tic Tac Toe</h1>

      {gameStart === false ? (
        <button
          onClick={() => dispatch(startGame())}
          className="TicTac__button"
        >
          Start
        </button>
      ) : (
        <>
          <ul className="TicTac__list">
            <li className="TicTac__list--item">X's Score: {scoreX}</li>
            <li className="TicTac__list--item">O's Score: {scoreO}</li>
          </ul>

          <button
            onClick={() => dispatch(startGame())}
            className="TicTac__button"
          >
            Stop
          </button>

          {scoreX !== 0 || scoreO !== 0 ? (
            <button
              onClick={() => dispatch(resetScores())}
              className="TicTac__button"
            >
              Reset Scores
            </button>
          ) : null}
        </>
      )}

      {currentTurn === 'X' && gameStart === true ? (
        <p className="TicTac__text">Next Turn: O</p>
      ) : null}
      {currentTurn === 'O' && gameStart === true ? (
        <p className="TicTac__text">Next Turn: X</p>
      ) : null}

      {tie ? (
        <div>
          <h1 className="TicTac__text">It's a Tie!!</h1>
        </div>
      ) : null}

      {winner === true && gameStart ? (
        <div>
          <h1 className="TicTac__text">{currentTurn}'s Win!!</h1>
        </div>
      ) : null}

      <div className="TicTac__container">
        <div className="TicTac__board">
          {gameStart === true
            ? board.map((el, index) => (
                <div className="TicTac__board--box">
                  <button
                    className="TicTac__board--button"
                    key={index}
                    onClick={() => {
                      dispatch(setX(index));
                    }}
                  >
                    {el}
                  </button>
                </div>
              ))
            : null}
        </div>
      </div>
    </div>
  );
}
