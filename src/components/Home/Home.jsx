import React from 'react';
import { Link } from 'react-router-dom';

import sudoku from '../../images/sudoku.png';
import hangman from '../../images/hangman.png';
import ttt from '../../images/game.png';

import './Home.scss';

export default function Home() {
  return (
    <div className="Home">
      <h1 className="Home__title">Let's Play</h1>

      <div className="Home__rulebook">
        <div>
          <h3>Sudoku</h3>
          <Link to="/sudoku" className="Home__images">
            <img src={sudoku} alt="sudoku" />
          </Link>
          <p>
            The rules of the game are simple: each of the nine blocks has to
            contain all the numbers 1-9 within its squares. Each number can only
            appear once in a row, column or box.
          </p>
        </div>
        <div>
          <h3>The Hangman</h3>
          <Link to="/hangman" className="Home__images">
            <img src={hangman} alt="hangman" />
          </Link>
          <p>
            One player thinks of a word or phrase; the others try to guess what
            it is one letter at a time. The player draws a number of dashes
            equivalent to the number of letters in the word. If a guessing
            player suggests a letter that occurs in the word, the other player
            fills in the blanks with that letter in the right places. If the
            word does not contain the suggested letter, the other player draws
            one element of a hangman’s gallows. As the game progresses, a
            segment of the gallows and of a victim is added for every suggested
            letter not in the word. The number of incorrect guesses before the
            game ends is up to the players, but completing a character in a
            noose provides a minimum of six wrong answers until the game ends.
            The first player to guess the correct answer thinks of the word for
            the next game.
          </p>
        </div>
        <div>
          <h3>Tic-Tac-Toe</h3>
          <Link to="/tic-tac-toe" className="Home__images">
            <img src={ttt} alt="tictactoe" />
          </Link>
          <p>
            The object of Tic Tac Toe is to get three in a row. You play on a
            three by three game board. The first player is known as X and the
            second is O. Players alternate placing Xs and Os on the game board
            until either oppent has three in a row or all nine squares are
            filled. X always goes first, and in the event that no one has three
            in a row, the stalemate is called a cat game.
          </p>
        </div>
      </div>
    </div>
  );
}
