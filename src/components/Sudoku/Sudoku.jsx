import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

import {
  selectIsStarted,
  selectGameBoard,
  startGame,
  stopGame,
} from '../../features/sudokuSlice';

import './Sudoku.scss';

export default function Sudoku() {
  const isStarted = useSelector(selectIsStarted);
  const gameBoard = useSelector(selectGameBoard);
  const dispatch = useDispatch();

  return (
    <div className="Sudoku">
      <h1 className="Sudoku__title">Sudoku</h1>

      {!isStarted ? (
        <button
          onClick={() => dispatch(startGame())}
          className="Sudoku__button"
        >
          New Game
        </button>
      ) : (
        <button onClick={() => dispatch(stopGame())} className="Sudoku__button">
          Stop
        </button>
      )}

      {isStarted ? (
        <div className="Sudoku__container">
          {gameBoard.map((value, index) => (
            <div key={index + value} className="Sudoku__cell">
              <input
                type="text"
                value={value}
                onChange={() => {}}
                className="Sudoku__cell--input"
              />
            </div>
          ))}
        </div>
      ) : null}

      <p>Under Construction</p>
    </div>
  );
}
