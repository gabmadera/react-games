import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  startGame,
  selectAnswer,
  selectStart,
  selectMistake,
  selectGuess,
  attemptGuess,
  stopGame,
  checkWin,
  selectIsWinner,
  selectIsLoading,
  stopLoading,
} from '../../features/hangmanSlice';
import Loader from 'react-loader-spinner';

import step0 from '../../images/step0.png';
import step1 from '../../images/step1.png';
import step2 from '../../images/step2.png';
import step3 from '../../images/step3.png';
import step4 from '../../images/step4.png';
import step5 from '../../images/step5.png';
import step6 from '../../images/step6.png';

import './Hangman.scss';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';

export default function Hangman() {
  const images = [step0, step1, step2, step3, step4, step5, step6];

  const answer = useSelector(selectAnswer);
  const isStarted = useSelector(selectStart);
  const mistake = useSelector(selectMistake);
  const dispatch = useDispatch();
  const guessed = useSelector(selectGuess);
  const isWinner = useSelector(selectIsWinner);
  const isLoading = useSelector(selectIsLoading);

  const gameOver = mistake >= 6;

  const guessedWord = () => {
    return answer
      .split('')
      .map((letter) => (guessed.includes(letter) ? letter : '_ '));

    // console.log(
    //   answer
    //     .split('')
    //     .map((letter) => (guessed.includes(letter) ? letter : '_ '))
    // );
  };

  const generateButtons = () => {
    return 'abcdefghijklmnñopqrstuvwxyz'.split('').map((letter) => (
      <button
        className="Hangman__buttons"
        key={letter}
        value={letter}
        onClick={() => {
          dispatch(attemptGuess(letter));
        }}
        disabled={guessed.includes(letter) || gameOver || isWinner}
      >
        {letter}
      </button>
    ));
  };

  const dashWord = guessedWord(answer, guessed);

  useEffect(() => {
    if (guessed.length) {
      const joinWord = dashWord.join('');
      dispatch(checkWin(joinWord));
    }
  }, [guessed, dispatch, dashWord]);

  useEffect(() => {
    dispatch(stopLoading());
  }, [isLoading, dispatch]);

  return (
    <div className="Hangman">
      <h1 className="Hangman__title">The Hangman</h1>

      {isStarted === false ? (
        <>
          <button
            onClick={() => dispatch(startGame())}
            className="Hangman__button-game"
          >
            Start
          </button>
        </>
      ) : (
        <>
          <button
            onClick={() => dispatch(stopGame())}
            className="Hangman__button-game"
          >
            Stop
          </button>

          <h3 className="Hangman__subtitle">Guess the word!</h3>
          <span className="Hangman__help">{answer}</span>
          {isWinner ? <h1 className="Hangman__over">Congrats!!!</h1> : null}
          {gameOver ? <h1 className="Hangman__over">Game Over!</h1> : null}

          <p>You have {mistake} mistakes from 6</p>

          <div>
            <img src={images[mistake]} alt="hangman_step" />
          </div>

          <div>
            <p className="Hangman__answer">
              {!gameOver ? guessedWord() : answer}
            </p>
            <div className="Hangman__letters">{generateButtons()}</div>
          </div>
        </>
      )}
    </div>
  );
}
