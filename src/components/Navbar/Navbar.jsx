import React from 'react';
import { Link } from 'react-router-dom';

import profile from '../../images/cv_picture.png';
import gitlab from '../../images/gitlab.png';

import './Navbar.scss';

export default function Navbar() {
  return (
    <nav className="Navbar">
      <ul className="Navbar__list">
        <li className="Navbar__list--item">
          <Link to="/" className="Navbar__link">
            <h3>Home</h3>
          </Link>
        </li>
        <li className="Navbar__list--item">
          <Link to="/tic-tac-toe" className="Navbar__link">
            <h3>Tic Tac Toe</h3>
          </Link>
        </li>
        <li className="Navbar__list--item">
          <Link to="/hangman" className="Navbar__link">
            <h3>Hangman</h3>
          </Link>
        </li>
        <li className="Navbar__list--item">
          <Link to="/sudoku" className="Navbar__link">
            <h3>Sudoku</h3>
          </Link>
        </li>
      </ul>

      <div className="Navbar__image-wrapper">
        <img src={profile} alt="profile" className="Navbar__image" />
        <p className="Navbar__text">By Gabriel Madera</p>
        <div className="Navbar__contact">
          <img src={gitlab} alt="gitlab_icon" className="Navbar__icon" />
          <a
            href="https://gitlab.com/gabmadera"
            className="Navbar__contact--link"
          >
            {'    '}
            @gabmadera{' '}
          </a>
        </div>
      </div>
    </nav>
  );
}
