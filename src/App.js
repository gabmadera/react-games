import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Navbar from './components/Navbar';
import Home from './components/Home';
import TicTac from './components/TicTac';
import Sudoku from './components/Sudoku';
import Hangman from './components/Hangman';

import './App.scss';

function App() {
  return (
    <Router>
      <div className="App">
        <Navbar />
        <Switch>
          <Route exact path="/tic-tac-toe" render={() => <TicTac />} />
          <Route exact path="/hangman" render={() => <Hangman />} />
          <Route exact path="/sudoku" render={() => <Sudoku />} />
          <Route exact path="/" render={() => <Home />} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
