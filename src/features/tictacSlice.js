import { createSlice } from '@reduxjs/toolkit';
import checkWinner from '../utils/CheckWinner';

export const tictacSlice = createSlice({
  name: 'tictac',
  initialState: {
    isStarted: false,
    board: Array(9).fill(null),
    turn: 'X',
    scoreX: 0,
    scoreO: 0,
    winner: false,
    tie: false,
  },
  reducers: {
    startGame: (state) => {
      state.isStarted = !state.isStarted;
      state.board = Array(9).fill(null);
      state.winner = false;
    },
    nextTurn: (state) => {
      if (state.isStarted) {
        state.turn = state.turn === 'O' ? 'X' : 'O';
      }
    },
    setX: (state, action) => {
      if (state.isStarted) {
        state.turn = state.turn === 'X' ? 'O' : 'X';
        state.board[action.payload] = state.turn;
      }
    },
    checkWin: (state) => {
      const isWinner = checkWinner(state.board);

      if (isWinner) {
        // state.isStarted = !state.isStarted;
        state.board = Array(9).fill(null);

        if (state.turn === 'X') {
          state.scoreX += 1;
        }
        if (state.turn === 'O') {
          state.scoreO += 1;
        }

        state.winner = true;
      }

      if (!state.board.includes(null)) {
        state.tie = true;
        state.board = Array(9).fill(null);

        // console.log('tie', state.tie);
      }
    },
    resetScores: (state, action) => {
      state.scoreO = 0;
      state.scoreX = 0;
      state.winner = false;
    },
  },
});

// Actions:
export const {
  startGame,
  nextTurn,
  setX,
  checkWin,
  resetScores,
} = tictacSlice.actions;

// Selectors:
export const selectGame = (state) => state.tictac.isStarted;
export const selectTurn = (state) => state.tictac.turn;
export const selectBoard = (state) => state.tictac.board;
export const selectScoreX = (state) => state.tictac.scoreX;
export const selectScoreO = (state) => state.tictac.scoreO;
export const selectTie = (state) => state.tictac.tie;
export const selectIsWinner = (state) => state.tictac.winner;

export default tictacSlice.reducer;
