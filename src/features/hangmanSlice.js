import { createSlice } from '@reduxjs/toolkit';
import randomWord from '../utils/HangmanWords';

export const hangmanSlice = createSlice({
  name: 'hangman',
  initialState: {
    isStarted: false,
    mistake: 0,
    guessed: [],
    answer: randomWord(),
    isWinner: false,
    isLoser: false,
    isLoading: false,
  },
  reducers: {
    startGame: (state) => {
      state.isStarted = !state.isStarted;
      state.answer = randomWord();
      state.isLoading = true;
      console.log(state.isLoading);
    },
    stopGame: (state) => {
      state.isStarted = false;
      state.answer = randomWord();
      state.guessed = [];
      state.mistake = 0;
      state.isWinner = false;
      state.isLoading = false;
    },
    attemptGuess: (state, action) => {
      const letter = action.payload;

      if (!state.guessed.includes(letter)) {
        state.guessed.push(letter);
        // console.log(state.guessed);

        if (!state.answer.includes(letter)) {
          state.mistake += 1;
        }
      }
    },
    checkWin: (state, action) => {
      const dashWord = action.payload;

      if (state.mistake >= 6) {
        state.isLoser = true;
        return;
      }

      if (!state.isWinner && state.answer === dashWord) {
        state.isWinner = true;
      }
    },
    stopLoading: (state) => {
      if (state.isLoading) {
        setTimeout(() => {
          // TODO return (state.isLoading = false);
        }, 2000);
      }
    },
  },
});

// Actions
export const {
  startGame,
  stopGame,
  attemptGuess,
  checkWin,
  stopLoading,
} = hangmanSlice.actions;

// Selectors
export const selectAnswer = (state) => state.hangman.answer;
export const selectStart = (state) => state.hangman.isStarted;
export const selectMistake = (state) => state.hangman.mistake;
export const selectGuess = (state) => state.hangman.guessed;
export const selectIsWinner = (state) => state.hangman.isWinner;
export const selectIsLoading = (state) => state.hangman.isLoading;

export default hangmanSlice.reducer;
