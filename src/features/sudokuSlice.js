import { createSlice } from '@reduxjs/toolkit';

import { makepuzzle, solvepuzzle } from 'sudoku';

export const sudokuSlice = createSlice({
  name: 'sudoku',
  initialState: {
    isStarted: false,
    gameBoard: [],
    solutionBoard: [],
  },
  reducers: {
    startGame: (state) => {
      state.isStarted = true;

      const puzzle = makepuzzle();
      state.gameBoard = puzzle;

      const solution = solvepuzzle();
      state.solutionBoard = solution;
    },
    stopGame: (state) => {
      state.isStarted = false;
    },
  },
});

// Actions
export const { startGame, stopGame } = sudokuSlice.actions;

// Selectors
export const selectIsStarted = (state) => state.sudoku.isStarted;
export const selectGameBoard = (state) => state.sudoku.gameBoard;
export const selectSolutionBoard = (state) => state.sudoku.solutionBoard;

export default sudokuSlice.reducer;
