export default function randomWord() {
  const words = [
    'symfony',
    'html',
    'javascript',
    'angular',
    'react',
    'upgrade',
    'ubuntu',
    'node',
    'mongodb',
    'mysql',
    'python',
    'flutter',
  ];

  return words[Math.floor(Math.random() * words.length)];
}
