export default function checkRowWinner(board) {
  let isWinner = false;
  let cell1 = null;
  let cell2 = null;
  let cell3 = null;
  // console.log(isWinner);

  const rowWin1 = [0, 1, 2];
  const rowWin2 = [3, 4, 5];
  const rowWin3 = [6, 7, 8];
  const rowWin4 = [0, 3, 6];
  const rowWin5 = [1, 4, 7];
  const rowWin6 = [2, 5, 8];
  const rowWin7 = [0, 4, 8];
  const rowWin8 = [2, 4, 6];

  for (let i = 0; i < rowWin1.length; i++) {
    if (i === 0) {
      cell1 = board[rowWin1[i]];
    }
    if (i === 1) {
      cell2 = board[rowWin1[i]];
    }
    if (i === 2) {
      cell3 = board[rowWin1[i]];
    }
  }

  if (cell1 && cell2 && cell3 && cell1 === cell2 && cell2 === cell3) {
    isWinner = true;

    return isWinner;
  }

  for (let i = 0; i < rowWin2.length; i++) {
    if (i === 0) {
      cell1 = board[rowWin2[i]];
    }
    if (i === 1) {
      cell2 = board[rowWin2[i]];
    }
    if (i === 2) {
      cell3 = board[rowWin2[i]];
    }
  }

  if (cell1 && cell2 && cell3 && cell1 === cell2 && cell2 === cell3) {
    isWinner = true;

    return isWinner;
  }

  for (let i = 0; i < rowWin3.length; i++) {
    if (i === 0) {
      cell1 = board[rowWin3[i]];
    }
    if (i === 1) {
      cell2 = board[rowWin3[i]];
    }
    if (i === 2) {
      cell3 = board[rowWin3[i]];
    }
  }

  if (cell1 && cell2 && cell3 && cell1 === cell2 && cell2 === cell3) {
    isWinner = true;

    return isWinner;
  }

  for (let i = 0; i < rowWin4.length; i++) {
    if (i === 0) {
      cell1 = board[rowWin4[i]];
    }
    if (i === 1) {
      cell2 = board[rowWin4[i]];
    }
    if (i === 2) {
      cell3 = board[rowWin4[i]];
    }
  }

  if (cell1 && cell2 && cell3 && cell1 === cell2 && cell2 === cell3) {
    isWinner = true;

    return isWinner;
  }

  for (let i = 0; i < rowWin5.length; i++) {
    if (i === 0) {
      cell1 = board[rowWin5[i]];
    }
    if (i === 1) {
      cell2 = board[rowWin5[i]];
    }
    if (i === 2) {
      cell3 = board[rowWin5[i]];
    }
  }

  if (cell1 && cell2 && cell3 && cell1 === cell2 && cell2 === cell3) {
    isWinner = true;

    return isWinner;
  }

  for (let i = 0; i < rowWin6.length; i++) {
    if (i === 0) {
      cell1 = board[rowWin6[i]];
    }
    if (i === 1) {
      cell2 = board[rowWin6[i]];
    }
    if (i === 2) {
      cell3 = board[rowWin6[i]];
    }
  }

  if (cell1 && cell2 && cell3 && cell1 === cell2 && cell2 === cell3) {
    isWinner = true;
    return isWinner;
  }

  for (let i = 0; i < rowWin7.length; i++) {
    if (i === 0) {
      cell1 = board[rowWin7[i]];
    }
    if (i === 1) {
      cell2 = board[rowWin7[i]];
    }
    if (i === 2) {
      cell3 = board[rowWin7[i]];
    }
  }

  if (cell1 && cell2 && cell3 && cell1 === cell2 && cell2 === cell3) {
    isWinner = true;

    return isWinner;
  }

  for (let i = 0; i < rowWin8.length; i++) {
    if (i === 0) {
      cell1 = board[rowWin8[i]];
    }
    if (i === 1) {
      cell2 = board[rowWin8[i]];
    }
    if (i === 2) {
      cell3 = board[rowWin8[i]];
    }
  }

  if (cell1 && cell2 && cell3 && cell1 === cell2 && cell2 === cell3) {
    isWinner = true;

    return isWinner;
  }

  return isWinner;
  // console.log(tie);
}
