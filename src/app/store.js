import { configureStore } from '@reduxjs/toolkit';

import tictacReducer from '../features/tictacSlice';
import hangmanReducer from '../features/hangmanSlice';
import sudokuReducer from '../features/sudokuSlice';

export default configureStore({
  reducer: {
    tictac: tictacReducer,
    hangman: hangmanReducer,
    sudoku: sudokuReducer,
  },
});
